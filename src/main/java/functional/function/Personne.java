package functional.function;

import java.util.ArrayList;
import java.util.List;

public class Personne {

    private String nom;
    private int age;
    private List<Personne> personnes = new ArrayList<>();

    public Personne(String nom, int age) {
        this.nom = nom;
        this.age = age;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public void addPersonne(Personne personne) {
        personnes.add(personne);
    }
    
    public List<Personne> getPersonnes() {
        List<Personne> autres = new ArrayList<>();
        autres.addAll(personnes);
        autres.add(this);
        return autres;
    }

    @Override
    public String toString() {
        return "Personne [nom=" + nom + ", age=" + age + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + age;
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Personne other = (Personne) obj;
        if (age != other.age)
            return false;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        return true;
    }
    
}
