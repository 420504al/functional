package functional.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionTRExample {

    public static void main(String args[]) {

        Function<Personne, String> funcPersonneToString = Personne::getNom;

        List<Personne> employeeList = Arrays.asList(new Personne("Jay Faim", 45), new Personne("Raihau Raihau", 25),
                new Personne("Maxime Pigeon", 65), new Personne("Gabriel Labbé", 15), new Personne("Ivan Dimitri", 29));

        List<String> empNameList = convertPersonneListToNamesList(employeeList, funcPersonneToString);

        empNameList.forEach(System.out::println);
    }

    public static List<String> convertPersonneListToNamesList(List<Personne> employeeList,
            Function<Personne, String> funcPersonneToString) {

        List<String> personneNameList = new ArrayList<String>();

        for (Personne personne : employeeList) {
            personneNameList.add(funcPersonneToString.apply(personne));
        }

        return personneNameList;
    }
}
