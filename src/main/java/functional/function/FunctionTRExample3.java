package functional.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionTRExample3 {

    public static void main(String args[]) {

        Function<Personne, String> funcEmpToString = Personne::getNom;
        Function<Personne, Personne> funcPersonnePrenom = FunctionTRExample3::extractPrenom;

        List<Personne> employeeList = 
                Arrays.asList(
                        new Personne("Jay Faim", 45), 
                        new Personne("Raihau Raihau", 25),
                        new Personne("Maxime Pigeon", 65), 
                        new Personne("Gabriel Labbé", 15), 
                        new Personne("Ivan Dimitri", 29));

        List<String> personneList = convertPersonneListToNamesList(employeeList,
                funcEmpToString.compose(funcPersonnePrenom));

        personneList.forEach(System.out::println);
    }
    public static List<String> convertPersonneListToNamesList(List<Personne> personneList,
            Function<Personne, String> funcPersonneToString) {
        
        List<String> personneNameList = new ArrayList<String>();
        personneList.forEach(p -> personneNameList.add(funcPersonneToString.apply(p)));
        return personneNameList;
    }

    public static Personne extractPrenom(Personne personne) {
        int index = personne.getNom().indexOf(" ");
        String firstName = personne.getNom().substring(0, index);
        personne.setNom(firstName);
        return personne;
    }
    
}
