package functional.function;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionTRExample2{
    
  public static void main(String args[]){
    Function<Personne, String> funcPersonneToString = Personne::getNom;
    Function<String, String> extractString = s -> s.substring(0,1);
    
    List<Personne> personneList= 
     Arrays.asList(new Personne("Jay Faim", 45), 
      new Personne("Raihau Raihau", 25),
      new Personne("Maxime Pigeon", 65),
      new Personne("Gabriel Labbé", 15),
      new Personne("Ivan Dimitri", 29));
    
    
    List<String> personneNameList = convertPersonneListToNamesList(
            personneList, funcPersonneToString.andThen(extractString));
    
    personneNameList.forEach(System.out::println);
 }
 public static List<String> convertPersonneListToNamesList(
         List<Personne> employeeList, Function<Personne, 
         String> funcPersonneToString){
     
   List<String> personneNameList=new ArrayList<String>(); 
   for(Personne personne:employeeList){
     personneNameList.add(funcPersonneToString.apply(personne));
   }
   return personneNameList;
  }
}
