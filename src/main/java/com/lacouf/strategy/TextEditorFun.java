package com.lacouf.strategy;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class TextEditorFun {

    public static void main(String[] args) {
        main2();
    }

    private static void main1() {
        System.out.println("With ErrorTextFormatter");
        publishText("ERROR - something bad happened", s -> s.startsWith("ERROR"), s -> s.toUpperCase());
        publishText("DEBUG - I'm here", s -> s.startsWith("ERROR"), s -> s.toUpperCase());

        System.out.println("\nWith PlainTextFormatter");
        publishText("ERROR - something bad happened", s -> true, s -> s);
        publishText("DEBUG - I'm here", s -> true, s -> s);

        System.out.println("\nWith ShortTextFormatter");
        publishText("ERROR - something bad happened", s -> s.length() < 20, s -> s.toLowerCase());
        publishText("DEBUG - I'm here", s -> s.length() < 20, s -> s.toLowerCase());
    }
    
    private static void main2() {
        System.out.println("With ErrorTextFormatter");
        publishText("ERROR - something bad happened", TextUtil::acceptErrors, TextUtil::formatError);
        publishText("DEBUG - I'm here", TextUtil::acceptErrors, TextUtil::formatError);

        System.out.println("\nWith PlainTextFormatter");
        publishText("ERROR - something bad happened", TextUtil::acceptAll, TextUtil::noFormatting);
        publishText("DEBUG - I'm here", TextUtil::acceptAll, TextUtil::noFormatting);

        System.out.println("\nWith ShortTextFormatter");
        publishText("ERROR - something bad happened", TextUtil::shortText, TextUtil::formatShort);
        publishText("DEBUG - I'm here", TextUtil::shortText, TextUtil::formatShort);
    }

    public static void publishText(String text, Predicate<String> filter, UnaryOperator<String> format) {
        if (filter.test(text)) {
            System.out.println(format.apply(text));
        }
    }
    
    static class TextUtil {
        public static boolean acceptAll(String text) {
            return true;
        }
     
        public static boolean acceptErrors(String text) {
            return text.startsWith( "ERROR" );
        }
        
        public static boolean shortText(String text) {
            return text.length() < 20;
        }

        public static String noFormatting(String text) {
            return text;
        }
     
        public static String formatError(String text) {
            return text.toUpperCase();
        }
        
        public static String formatShort(String text) {
            return text.toLowerCase();
        }
        
    }
}
