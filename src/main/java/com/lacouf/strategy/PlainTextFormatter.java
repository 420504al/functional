package com.lacouf.strategy;

public class PlainTextFormatter implements TextFormatter {
    
    @Override
    public boolean filter( String text ) {
        return true;
    }
 
    @Override
    public String format( String text ) {
        return text;
    }
}
