package com.lacouf.strategy;

public interface TextFormatter {
    boolean filter(String text);
    String format(String text);
}
