package com.lacouf.strategy;

public class TextEditorDemo {
    public static void main(String[] args) {
        System.out.println("With ErrorTextFormatter");
        TextEditor textEditorError = new TextEditor( new ErrorTextFormatter() );
        textEditorError.publishText( "ERROR - something bad happened" );
        textEditorError.publishText( "DEBUG - I'm here" );
        
        System.out.println("\nWith PlainTextFormatter");
        TextEditor textEditorPlain = new TextEditor( new PlainTextFormatter() );
        textEditorPlain.publishText( "ERROR - something bad happened" );
        textEditorPlain.publishText( "DEBUG - I'm here" );
        
        System.out.println("\nWith ShortTextFormatter");
        TextEditor textEditorShort = new TextEditor( new ShortTextFormatter() );
        textEditorShort.publishText( "ERROR - something bad happened" );
        textEditorShort.publishText( "DEBUG - I'm here" );

    }
}
