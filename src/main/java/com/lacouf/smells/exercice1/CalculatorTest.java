package com.lacouf.smells.exercice1;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CalculatorTest {

    @Test
    public void testCalculatorDefault() {
        Calculator calc = new Calculator();
        Money calculate = calc.calculate(60, 229, 162, 25, Currency.CAD);
        assertThat(calculate.toString()).isEqualTo("Money [currency=CAD, amount=120.0]");
    }
    
    @Test
    public void testCalculatorExceed1() {
        Calculator calc = new Calculator();
        Money calculate = calc.calculate(61, 229, 162, 25, Currency.CAD);
        assertThat(calculate.toString()).isEqualTo("Money [currency=CAD, amount=244.0]");
    }
    
    @Test
    public void testCalculatorExceed2() {
        Calculator calc = new Calculator();
        Money calculate = calc.calculate(60, 230, 162, 25, Currency.CAD);
        assertThat(calculate.toString()).isEqualTo("Money [currency=CAD, amount=240.0]");
    }
    
    @Test
    public void testCalculatorExceed3() {
        Calculator calc = new Calculator();
        Money calculate = calc.calculate(60, 229, 163, 25, Currency.CAD);
        assertThat(calculate.toString()).isEqualTo("Money [currency=CAD, amount=240.0]");
    }
    
    @Test
    public void testCalculatorExceed4() {
        Calculator calc = new Calculator();
        Money calculate = calc.calculate(60, 229, 162, 26, Currency.CAD);
        assertThat(calculate.toString()).isEqualTo("Money [currency=CAD, amount=240.0]");
    }
    
    @Test
    public void testCalculatorUSD() {
        Calculator calc = new Calculator();
        Money calculate = calc.calculate(60, 229, 162, 25, Currency.USD);
        assertThat(calculate.toString()).isEqualTo("Money [currency=USD, amount=156.0]");
    }
    
    @Test
    public void testCalculatorEUR() {
        Calculator calc = new Calculator();
        Money calculate = calc.calculate(60, 229, 162, 25, Currency.EUR);
        assertThat(calculate.toString()).isEqualTo("Money [currency=EUR, amount=180.0]");
    }

}
