package com.lacouf.smells.exercice1;

public class Calculator {
    
    public Money calculate(int weight, int height, int width, int depth, Currency currency) {
        double postageInBaseCurrency = postageInBaseCurrency(weight, height, width, depth);
        return convertCurrency(postageInBaseCurrency, currency);
    }

    private double postageInBaseCurrency(int weight2, int height2, int width2, int depth2) {
        if (weight2 <= 60 && height2 <= 229 && width2 <= 162 && depth2 <= 25)
            return 120;
        
        if (weight2 <= 500 && height2 <= 324 && width2 <= 229 && depth2 <= 100)
            return weight2 * 4;
        
        return Math.max(weight2, (height2 * width2 * depth2/1000) * 6);
    }

    private Money convertCurrency(double postageInBaseCurrency, Currency currency) {
        if (currency == Currency.CAD)
            return new Money(Currency.CAD, postageInBaseCurrency);
        if (currency == Currency.USD)
            return new Money(Currency.USD, postageInBaseCurrency * 1.3);
        if (currency == Currency.EUR)
            return new Money(Currency.EUR, postageInBaseCurrency * 1.5);
        throw new RuntimeException("Unhandled currency");
    }
    
}
