package com.lacouf.smells.exercice1.refactored;

public class Calculator {
    
    public Money calculate(int weight, int height, int width, int depth, Currency currency) {
        double postagePrice = SizedPackage.createSizedPackage(weight, height, width, depth).postageInBaseCurrency();
        return convertCurrency(postagePrice, currency);
    }

    private Money convertCurrency(double postageInBaseCurrency, Currency currency) {
        if (currency == Currency.CAD)
            return new Money(Currency.CAD, postageInBaseCurrency);
        if (currency == Currency.USD)
            return new Money(Currency.USD, postageInBaseCurrency * 1.3);
        if (currency == Currency.EUR)
            return new Money(Currency.EUR, postageInBaseCurrency * 1.5);
        throw new RuntimeException("Unhandled currency");
    }
    
}
