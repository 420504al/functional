package com.lacouf.smells.exercice1.refactored;

public class LargePackage extends SizedPackage {
    private int weight;
    private int height;
    private int width;
    private int depth;

    public LargePackage(int weight, int height, int width, int depth) {
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.depth = depth;
    }

    public double packagePostageInBaseCurrency() {
        return Math.max(weight, (height * width * depth/1000) * 6);
    }
}
