package com.lacouf.smells.exercice1.refactored;

public class MediumPackage extends SizedPackage {

    private int weight;

    public MediumPackage(int weight) {
        this.weight = weight;
    }

    public double packagePostageInBaseCurrency() {
        return weight * 4;
    }

}
