package com.lacouf.smells.exercice1.refactored;

public class SmallPackage extends SizedPackage {

    public double packagePostageInBaseCurrency() {
        return 120f;
    }
}