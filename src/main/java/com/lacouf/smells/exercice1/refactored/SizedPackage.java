package com.lacouf.smells.exercice1.refactored;

public abstract class SizedPackage {

    public SizedPackage() {
        super();
    }

    public abstract double packagePostageInBaseCurrency();

    public double postageInBaseCurrency() {
        return packagePostageInBaseCurrency();
    }

    public static SizedPackage createSizedPackage(int weight, int height, int width, int depth) {
        if (isSmallPackage(weight, height, width, depth))
            return new SmallPackage();
        
        if (isMediumPackage(weight, height, width, depth))
            return new MediumPackage(weight);
        
        return new LargePackage(weight, height, width, depth);
    }

    private static boolean isMediumPackage(int weight, int height, int width, int depth) {
        return weight <= 500 && height <= 324 && width <= 229 && depth <= 100;
    }

    private static boolean isSmallPackage(int weight, int height, int width, int depth) {
        return weight <= 60 && height <= 229 && width <= 162 && depth <= 25;
    }

}