package com.lacouf.smells.exercice1;

public class Money {
    private final Currency currency;
    private final double amount;

    public Money(Currency currency, double amount) {
        this.currency = currency;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Money [currency=" + currency + ", amount=" + amount + "]";
    }
}
