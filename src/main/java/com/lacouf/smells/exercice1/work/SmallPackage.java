package com.lacouf.smells.exercice1.work;

public class SmallPackage extends Package {

    public double postageInBaseCurrency() {
        return 120d;
    }

}
