package com.lacouf.smells.exercice1.work;

public class MediumPackage extends Package {
    
    private int weight;

    public MediumPackage(int weight) {
        this.weight = weight;
    }

    public double postageInBaseCurrency() {
        return weight * 4;
    }
}
