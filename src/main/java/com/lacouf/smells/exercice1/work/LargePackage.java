package com.lacouf.smells.exercice1.work;

public class LargePackage extends Package {

    private int weight;
    private int height;
    private int width;
    private int depth;

    public LargePackage(int weight, int height, int width, int depth) {
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.depth = depth;
    }
    
    public double postageInBaseCurrency() {
        return Math.max(weight, (height * width * depth/1000) * 6);
    }

}
