package com.lacouf.exercice;

import java.util.stream.IntStream;

public class KonstantinExercice {
    
    public static void main(String[] args) {
        IntStream.rangeClosed(1, 100)
                 .filter(i -> i % 2 == 0)
                 .forEach(System.out::println);
    }

}
