package com.lacouf.templatemethod2;

public class Resource extends AbstractRessource {
    
    public void useResource(String commandToPass) {
        if (!state.equals("O"))
            throw new RuntimeException("invalid state");
        riskyOperation();
        System.out.println("Resource used");
    }

}
