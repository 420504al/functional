package com.lacouf.templatemethod2;

import java.util.Random;

public abstract class AbstractRessource {
    protected String state = "";
    
    public AbstractRessource() {
        System.out.println("Resource créé");
        state = "C";
    }
    
    public void execute(String command) {
        try {
            openResource();
            useResource(command);
        } catch (Exception e) {
            System.out.println("Exception!  Please redo");
        } finally {
            closeResource();
        }
    }
    
    public void openResource() {
        if (!state.equals("C"))
            throw new RuntimeException("invalid state");
        riskyOperation();
        state = "O";
        System.out.println("Resource open");
    }
    
    protected abstract void useResource(String command);
    
    public void closeResource() {
        System.out.println("Resource closed - MUST BE CALLED");
        state = "C";
    }
    
    protected void riskyOperation() {
        if (new Random().nextInt(10) == 0)
            throw new RuntimeException();
    }

}
