package com.lacouf.templatemethod2;

public class ResourceDemo {
    public static void main(String[] args) {
        new Resource().execute("Hello World!");
        new Resource().execute("Hello Jay");
        new Resource().execute("Hello Ivan");
    }
}
