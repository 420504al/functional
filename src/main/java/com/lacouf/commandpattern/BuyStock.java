package com.lacouf.commandpattern;

public class BuyStock extends Order{

    private Stock stock;
    
    public BuyStock(Stock abcStock) {
        this.stock = abcStock;
    }

    @Override
    public void execute() {
        stock.buy();
    }

    @Override
    public Stock getStock() {
        return stock;
    }
    
}
