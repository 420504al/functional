package com.lacouf.commandpattern;

public class Stock {
    
    private String name = "ABC";
    private int qty = 10;
    
    public Stock(String name, int qty) {
        this.name = name;
        this.qty = qty;
    }
    public String buy() {
        return "Stock [Name: " + name +
                " Quantity" + qty + "] bought";
    }
    
    public String sell() {
        return "Stock [Name: " + name +
                " Quantity" + qty + "] sold";
    }
    
    @Override
    public String toString() {
        return " [name=" + name + ", qty=" + qty + "]";
    }

}
