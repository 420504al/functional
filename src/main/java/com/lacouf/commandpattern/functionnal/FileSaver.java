package com.lacouf.commandpattern.functionnal;

public class FileSaver implements Runnable {
    public final String message;
 
    public FileSaver( String message ) {
        this.message = message;
    }
 
    @Override
    public void run() {
        System.out.println("Saving: " + message);
    }
}
