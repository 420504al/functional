package com.lacouf.commandpattern.functionnal;

public class Mailer implements Runnable {
    public final String message;
 
    public Mailer( String message ) {
        this.message = message;
    }
 
    @Override
    public void run() {
        System.out.println("Sending: " + message);
    }
}