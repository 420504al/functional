package com.lacouf.commandpattern.functionnal;

public class Logger implements Runnable {
    public final String message;
 
    public Logger( String message ) {
        this.message = message;
    }
 
    @Override
    public void run() {
        System.out.println("Logging: " + message);
    }
}