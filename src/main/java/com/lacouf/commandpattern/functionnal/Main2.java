package com.lacouf.commandpattern.functionnal;

import java.util.ArrayList;
import java.util.List;

public class Main2 {
    public static void main(String[] args) {
        List<Runnable> tasks = new ArrayList<>();
        tasks.add(() -> log("Hi"));
        tasks.add(() -> save("Cheers"));
        tasks.add(() -> mail("Bye"));
        execute(tasks);
    }
    
    public static void execute(List<Runnable> tasks) {
        tasks.forEach(Runnable::run);
    }
    
    private static void log(String message) {
        System.out.println("Logging: " + message);
    }
    
    private static void mail(String message) {
        System.out.println("Sending: " + message);
    }
    
    private static void save(String message) {
        System.out.println("Saving: " + message);
    }
    
}
