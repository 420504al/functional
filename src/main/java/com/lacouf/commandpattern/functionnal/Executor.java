package com.lacouf.commandpattern.functionnal;

import java.util.List;

public class Executor {
    public void execute(List<Runnable> tasks) {
        for (Runnable task : tasks) {
            task.run();
        }
    }
}
