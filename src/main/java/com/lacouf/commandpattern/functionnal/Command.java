package com.lacouf.commandpattern.functionnal;

public interface Command {
    
    void run();

}
