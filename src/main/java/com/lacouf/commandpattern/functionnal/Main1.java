package com.lacouf.commandpattern.functionnal;

import java.util.ArrayList;
import java.util.List;

public class Main1 {
    public static void main(String[] args) {
        List<Runnable> tasks = new ArrayList<>();
        tasks.add(new Logger( "Hi" ));
        tasks.add(new FileSaver( "Cheers" ));
        tasks.add(new Mailer( "Bye" ));
         
        new Executor().execute( tasks );
    }
}
