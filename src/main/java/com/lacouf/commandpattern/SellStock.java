package com.lacouf.commandpattern;

public class SellStock extends Order{
    
private Stock stock;
    
    public SellStock(Stock abcStock) {
        this.stock = abcStock;
    }

    @Override
    public void execute() {
        stock.sell();
    }

    @Override
    public Stock getStock() {
        return stock;
    }
}
