package com.lacouf.commandpattern;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Broker {
    private int txnNumber = 0;
    private List<Order> orderList = Collections.synchronizedList(new ArrayList<>());
    private List<StockTxn> stockTxns = Collections.synchronizedList(new ArrayList<>());
    
    public void takeOrder(Order order) {
        synchronized(orderList) {
            orderList.add(order);
        }
    }
    
    public void placeOrders() {
        synchronized (orderList) {
            for (Order order : orderList) {
                order.execute();
                synchronized(stockTxns) {
                    stockTxns.add(new StockTxn(txnNumber, order.getStock()));
                }
            }
            orderList.clear();
            txnNumber++;
        }
    }
    
    public void printAndEmptyTxnStockList() {
        synchronized (stockTxns) {
            for (StockTxn txn : stockTxns) {
                System.out.println("id:" + txn.getTxnNumber() + " Stock:" + txn.getStock());
            }
            stockTxns.clear();
        }
    }
}
