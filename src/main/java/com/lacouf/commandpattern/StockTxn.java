package com.lacouf.commandpattern;

public class StockTxn {
    private int txnNumber;
    private Stock stock;
    
    public StockTxn(int txnNumber, Stock stock) {
        super();
        this.txnNumber = txnNumber;
        this.stock = stock;
    }
    
    public int getTxnNumber() {
        return txnNumber;
    }
    public Stock getStock() {
        return stock;
    }

}
