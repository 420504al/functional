package com.lacouf.commandpattern;

public abstract class Order {
    
    public abstract void execute();

    public static Order create(String type, Stock stock) {
        switch(type) {
        case "buy":
            return new BuyStock(stock);
        case "sell":
            return new SellStock(stock);
        default:
            return null;
        }
    }

    public abstract Stock getStock();

}
