package com.lacouf.commandpattern;

public interface GarocheEvenementAction {
    
    public void go();

}
