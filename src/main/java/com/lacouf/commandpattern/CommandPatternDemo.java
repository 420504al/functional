package com.lacouf.commandpattern;

import java.util.Random;

public class CommandPatternDemo {
    public static void main(String[] args) {

        Broker broker = new Broker();

        GarocheurDEvenement takeCommandeGarocheur = new GarocheurDEvenement();
        takeCommandeGarocheur.addGarocheurEvenementListener(new GarocheEvenementAction() {
            @Override
            public void go() {
                System.out.println("take commande");
                String buyOrSell = new Random().nextBoolean() ? "buy" : "sell";
                broker.takeOrder(Order.create(buyOrSell, createRandomStock()));
            }
        });
        takeCommandeGarocheur.garoche(2);

        GarocheurDEvenement passeCommandeGarocheur = new GarocheurDEvenement();
        passeCommandeGarocheur.addGarocheurEvenementListener(new GarocheEvenementAction() {
            @Override
            public void go() {
                System.out.println("passe commande");
                broker.placeOrders();
            }
        });

        passeCommandeGarocheur.garoche(10);

        GarocheurDEvenement imprimeTransactionsGarocheur = new GarocheurDEvenement();
        imprimeTransactionsGarocheur.addGarocheurEvenementListener(new GarocheEvenementAction() {
            @Override
            public void go() {
                System.out.println("imprime commande");
                broker.printAndEmptyTxnStockList();
            }
        });

        imprimeTransactionsGarocheur.garoche(20);
    }

    private static Stock createRandomStock() {
        return new Stock(generateRandomString(3), new Random().nextInt(100));
    }

    private static String generateRandomString(int stringLength) {
        int leftLimit = 65; // letter 'A'
        int rightLimit = 65 + 26; // letter 'Z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++) {
            int randomLimitedInt = leftLimit + random.nextInt(rightLimit - leftLimit);
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

}
