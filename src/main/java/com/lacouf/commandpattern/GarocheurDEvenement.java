package com.lacouf.commandpattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GarocheurDEvenement {

    private static final int _1000MS = 1000;
    private ExecutorService exs = Executors.newFixedThreadPool(1);
    private List<GarocheEvenementAction> listeners = new ArrayList<>();

    public void addGarocheurEvenementListener(GarocheEvenementAction gea) {
        listeners.add(gea);
    }

    public void garoche(int randomDelayInSeconds) {
        exs.submit(() ->  {
                while (true) {
                    sleep(new Random().nextInt(randomDelayInSeconds));
                    notifyListeners();
                }
            }
        );
    }

    private void notifyListeners() {
        for (GarocheEvenementAction listener : listeners) {
            listener.go();
        }
    }

    private void sleep(int secondes) {
        try {
            Thread.sleep(secondes * _1000MS);
        } catch (Exception e) {
        }
    }
}
