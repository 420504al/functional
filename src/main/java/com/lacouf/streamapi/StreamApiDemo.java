package com.lacouf.streamapi;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

import functional.function.Personne;

public class StreamApiDemo {

    public static void main(String[] args) {
        Personne personne = new Personne("Jay Faim", 45);
        personne.addPersonne(new Personne("Jay Faim2", 45));
        
        List<List<Personne>> personneListe = 
                Arrays.asList(
                        personne.getPersonnes(),
                        new Personne("Jay Faim", 45).getPersonnes(),
                        new Personne("Raihau Raihau", 25).getPersonnes(),
                        new Personne("Maxime Pigeon", 65).getPersonnes(), 
                        new Personne("Gabriel Labbé", 15).getPersonnes(), 
                        new Personne("Ivan Dimitri", 29).getPersonnes());
        
        Predicate<PersonneDTO> adulte = p -> p.getAge() >= 18;
        
        System.out.println(personneListe.stream()
                     .flatMap(l -> l.stream())
                     .map(PersonneDTO::new)
                     .filter(p -> p.getAge() > 30)
                     .collect(toList())
                     )
                     ;
        
    }

    static class PersonneDTO {
        @Override
        public String toString() {
            return "PersonneDTO [nom=" + nom + ", age=" + age + "]";
        }

        private String nom;
        private int age;

        PersonneDTO(String nom, int age) {
            this.nom = nom;
            this.age = age;
        }
        
        PersonneDTO(Personne p) {
            this.nom = p.getNom();
            this.age = p.getAge();
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
        
    }
}
