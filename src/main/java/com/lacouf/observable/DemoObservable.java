package com.lacouf.observable;

public class DemoObservable {
    public static void main(String[] args) {
        
        Observable observable = new Observable();
        new Observer1(observable);
        new Observer2(observable);
        observable.sendEvent("Hello world!!!!");
    }
}
