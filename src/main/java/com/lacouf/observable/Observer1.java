package com.lacouf.observable;

public class Observer1 {

    public Observer1(Observable observable) {

        observable.register(this, new Listener() {

            @Override
            public void onEvent(Object event) {
                System.out.println(event);
            }
            
        });
    }
}
