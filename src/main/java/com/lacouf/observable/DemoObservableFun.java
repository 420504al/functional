package com.lacouf.observable;

public class DemoObservableFun {

    public static void main(String[] args) {
        ObservableFun observable = new ObservableFun();
        observable.register( "key1", e -> System.out.println(e) );
        observable.register( "key2", System.out::println );
        observable.sendEvent( "Hello World!" );
    }
}
