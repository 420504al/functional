package com.lacouf.observable;

public interface Listener {
    void onEvent(Object event);
}
