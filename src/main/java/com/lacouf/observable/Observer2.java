package com.lacouf.observable;

public class Observer2 implements Listener {
    
    public Observer2(Observable observable) {
        observable.register(this, this);
    }

    @Override
    public void onEvent(Object event) {
        System.out.println(event);
    }

}
