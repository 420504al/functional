package com.lacouf.observable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

public class ObservableFun {
    private final Map<Object, Consumer<Object>> listeners = new ConcurrentHashMap<>();
    
    public void register(Object key, Consumer<Object> listener) {
        listeners.put(key, listener);
    }
 
    public void unregister(Object key) {
        listeners.remove(key);
    }
 
    public void sendEvent(Object event) {
        listeners.values().forEach( listener -> listener.accept( event ) );
    }
}
