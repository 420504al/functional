package com.lacouf.builder;

import java.time.LocalDate;

import com.lacouf.builder.User.UserBuilder;

public class UserUsage {
    public static void main(String[] args) {
        User user = UserBuilder.createBuilder()
                    .withPrenom("Jeremie")
                    .withNom("Durand")
                    .withNaissance(LocalDate.of(2001, 2, 2))
                    .withNas("123456789")
                    .build();
        
        System.out.println(user);
    }
}
