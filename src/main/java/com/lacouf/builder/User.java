package com.lacouf.builder;

import java.time.LocalDate;

public class User {
    private String prenom;
    private String nom;
    private LocalDate naissance;
    private String nas;
    
    private User() {}
    

    @Override
    public String toString() {
        return "User [prenom=" + prenom + ", nom=" + nom + ", naissance=" + naissance + ", nas=" + nas + "]";
    }

    public static class UserBuilder {
        private User user;
        public static UserBuilder createBuilder() {
            return new UserBuilder(new User());
        }
        private UserBuilder(User user) {
            this.user = user;
        }
        public UserBuilder withPrenom(String prenom) {
            user.prenom = prenom;
            return this;
        }
        public UserBuilder withNom(String nom) {
            user.nom = nom;
            return this;
        }
        public UserBuilder withNaissance(LocalDate naissance) {
            user.naissance= naissance;
            return this;
        }
        public UserBuilder withNas(String nas) {
            user.nas = nas;
            return this;
        }
        public User build() {
            return user;
        }
    }
}
