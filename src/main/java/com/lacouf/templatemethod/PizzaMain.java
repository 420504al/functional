package com.lacouf.templatemethod;

public class PizzaMain {
    public static void main(String[] args) {
        PizzaFromageMince pfm = new PizzaFromageMince();
        pfm.preparePizza();
        
        Pizza padm = new PizzaAllDressedMince();
        padm.preparePizza();
    }

}
