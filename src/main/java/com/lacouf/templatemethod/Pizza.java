package com.lacouf.templatemethod;

public abstract class Pizza {

    public final void preparePizza() {
        ecrisNomPizza();
        choisirPate();
        choisirSauce();
        adjouteGarniture();
        cuire();
        livrer();
    }

    protected abstract void ecrisNomPizza();
    protected abstract void choisirPate();
    protected abstract void choisirSauce();
    protected abstract void adjouteGarniture();

    private void cuire() {
        System.out.println("cuire");
    }

    private void livrer() {
        System.out.println("livrer\n");
    }

}