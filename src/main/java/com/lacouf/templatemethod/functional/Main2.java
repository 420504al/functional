package com.lacouf.templatemethod.functional;

public class Main2 {

    public static void main(String[] args) {
        Resource resource = new Resource();
        try {
            resource.useResource();
            resource.employResource();
        } finally {
            resource.dispose();
        }
    }
}
