package com.lacouf.templatemethod.functional;

public class Main3 {
    public static void main(String[] args) {
        openResource();
        try {
            doSomethingWithResource();
        } finally {
           closeResource();
        }
    }

    private static void closeResource() {
    }

    private static void doSomethingWithResource() {
    }

    private static void openResource() {
    }
}
