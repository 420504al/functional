package com.lacouf.templatemethod.functional;

public class Main4 {

    public static void main(String[] args) {
        new ResourceUser().execute();
        new ResourceEmployer().execute();
    }
}
