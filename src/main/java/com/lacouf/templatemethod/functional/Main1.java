package com.lacouf.templatemethod.functional;

public class Main1 {

    public static void main(String[] args) {
        Resource resource = new Resource();
        resource.useResource();
        resource.employResource();
        resource.dispose();
    }
}
