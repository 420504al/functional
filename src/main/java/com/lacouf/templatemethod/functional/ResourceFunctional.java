package com.lacouf.templatemethod.functional;

import java.util.function.Consumer;

public class ResourceFunctional {
    
    public static void main(String[] args) {
        withResource( resource -> resource.useResource() );
        withResource( resource -> resource.employResource() );
    }
    
    public static void withResource( Consumer<Resource> consumer) {
        Resource resource = new Resource();
        try {
            consumer.accept( resource );
        } finally {
            resource.dispose();
        }
    }
}
