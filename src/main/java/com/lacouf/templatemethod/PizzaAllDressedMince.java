package com.lacouf.templatemethod;

public class PizzaAllDressedMince extends Pizza {
    
    protected void ecrisNomPizza() {
        System.out.println("Pizza All Dressed Mince");
    }

    protected void choisirPate() {
        System.out.println("Pate -> Pate mince");
    }

    protected void choisirSauce() {
        System.out.println("Sauce -> Sauce tomate");
    }

    protected void adjouteGarniture() {
        System.out.println("Garniture -> Fromage, Viande, Legume");
    }
}
