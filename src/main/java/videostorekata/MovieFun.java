package videostorekata;

public class MovieFun {

    private MovieTypeFun movieType;

    public MovieFun(MovieTypeFun movieType) {
        this.movieType = movieType;
    }

    public int calcPrice() {
        return movieType.getPrice();
    }
    
    public static void main(String[] args) {
        printMoviePrices();
    }

    private static void printMoviePrices() {
        MovieFun movie1 = new MovieFun(MovieTypeFun.REGULAR);
        MovieFun movie2 = new MovieFun(MovieTypeFun.NEW_RELEASE);
        MovieFun movie3 = new MovieFun(MovieTypeFun.ENFANT);
        System.out.println(movie1.calcPrice());
        System.out.println(movie2.calcPrice());
        System.out.println(movie3.calcPrice());
    }

}
