package videostorekata;

import java.util.function.Supplier;

public enum MovieTypeFun {
    REGULAR(() -> 1),
    NEW_RELEASE(() -> 3),
    ENFANT(() -> 2);
    
    private Supplier<Integer> priceStrategy;
    
    MovieTypeFun(Supplier<Integer> priceStrategy) {
        this.priceStrategy = priceStrategy;
    }

    public int getPrice() {
        return priceStrategy.get();
    }
}
